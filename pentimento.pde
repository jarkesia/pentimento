/*
    Copyright (C) 2017  Jari Suominen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import processing.net.*;
import processing.opengl.*;
import ddf.minim.*;
//import de.voidplus.myo.*;

//Myo myo;

Minim minim;
AudioPlayer player;

final boolean hueOn = false; // True if you are at the same network than philips hue hub. False if you wish to disable this feature.

Client c;
String data;

// http://192.168.1.33/api/22a395a83e6e63e482cf9196d48b686/lights
String apiKey = "22a395a83e6e63e482cf9196d48b686"; //developer name used when setting up bridge
String light = "1"; //the light # you want to control
String ip = "192.168.1.33";

PImage photo;
PImage glow;

int speed = 2;

float time = 0;

int colors[] = {
  42, 14, 31, // 0AM RGB
  19, 8, 15, 
  25, 9, 13, 
  38, 8, 12, 
  0, 0, 0, 
  213, 226, 71, 
  71, 226, 177, 
  70, 196, 227, 
  70, 162, 227, 
  70, 133, 227, 
  66, 87, 165, 
  48, 75, 174, 
  174, 158, 48, 
  213, 203, 36, 
  252, 236, 5, 
  247, 239, 127, 
  251, 246, 170, 
  189, 182, 174, 
  221, 158, 101, 
  226, 91, 50, 
  190, 83, 181, 
  190, 83, 146, 
  104, 35, 76, 
  74, 22, 53   //11PM
};

void settings() {
  //fullScreen();
  fullScreen(P2D, 1);
  //size(300, 200, P2D);
}

void setup() {
  //fullScreen();  
  noCursor();

  minim = new Minim(this);
  player = minim.loadFile("nightingale1.wav");
  player.loop();
  player.play();

 // myo = new Myo(this);
 //  myo.setFrequency(10);


  if (hueOn) c = new Client(this, ip, 80); // Connect to server on port 80

  photo = loadImage("tanssiaiskengat.jpg");
  glow = createImage(width, height, ARGB);
  glow = loadImage("glow.png");
  photo.resize(width, height);

  frameRate = 10;

  if (hueOn) {
    writeHSB(0, 255, 255, 9);
    writeHSB(23000, 255, 255, 12);
    writeHSB(45000, 255, 255, 1);
  }
}

int postR, postG, postB;

void draw() {

  background(0);
  imageMode(CORNER);

  time += (1.0*speed/2)*0.003; //(1.0*speed/10)*0.1;
  //time += 0.01;
  time %= 24;

  if (time < 5) player.unmute();
  else player.mute();

  rectMode(CORNERS);

  int r = (int)map(time - (int)time, 0, 1, colors[(int)time*3], colors[(((int)time+1)%24)*3]);
  int g = (int)map(time - (int)time, 0, 1, colors[(int)time*3+1], colors[(((int)time+1)%24)*3+1]);
  int b = (int)map(time - (int)time, 0, 1, colors[(int)time*3+2], colors[(((int)time+1)%24)*3+2]);

  //println(time + " R:" + r + " G:" + g + " B:" + b);


  tint(sin((time/24)*PI)*255, 255);
  image(photo, 0, 0, width, height);
  tint(r, g, b, 100);
  image(glow, 0, 0, width, height);

  stroke(70);
  rect((time/24)*width, 0, (time/24)*width+1, height);

  if (hueOn) {
    if (postR != r) {
      writeB(r, 9);
      postR = r;
    }
    if (postG != g) {
      writeB(g, 12);
      postG = g;
    }
    if (postB != b) {
      writeB(b, 1);
      postB = b;
    }
  }
}


void writeHSB(int h, int s, int b, int l) {

  writeB(b, l);

  c = new Client(this, ip, 80); // Connect to server on port 80
  c.write("PUT /api/" + apiKey +"/lights/" + l + "/state HTTP/1.1\r\n"); 
  c.write("Content-Length: " + 18 + str(s).length() + "\r\n\r\n");
  c.write("{\"sat\":" + s +"}\r\n");
  c.write("\r\n");
  c.stop();

  delay(1);

  c = new Client(this, ip, 80); // Connect to server on port 80
  c.write("PUT /api/" + apiKey +"/lights/" + l + "/state HTTP/1.1\r\n"); 
  c.write("Content-Length: " + 18 + str(h).length() + "\r\n\r\n");
  c.write("{\"hue\":" + h +"}\r\n");
  c.write("\r\n");
  c.stop();
  delay(1);
}

void writeB(int b, int l) {
  if (!c.active()) {
    c = new Client(this, ip, 80); // Connect to server on port 80
  }

  c.write("PUT /api/" + apiKey +"/lights/" + l + "/state HTTP/1.1\r\n"); 
  c.write("Content-Length: " + 8 + str(b).length() + "\r\n\r\n");
  c.write("{\"bri\":" + b +"}\r\n");
  //c.write("\n\n");
  c.stop();
}

void writeS(int b, int l) {
  if (!c.active()) {
    c = new Client(this, ip, 80); // Connect to server on port 80
  }

  c.write("PUT /api/" + apiKey +"/lights/" + l + "/state HTTP/1.1\r\n"); 
  c.write("Content-Length: " + 18 + str(b).length() + "\r\n\r\n");
  c.write("{\"sat\":" + b +"}\r\n");
  c.write("\r\n");
  // c.stop();
}

/*
void myoOnPose(Device myo, long timestamp, Pose pose) {

  switch (pose.getType()) {

  case WAVE_IN:
    myo.vibrate();
    if (speed < 2) speed++;
    break;
  case WAVE_OUT:
    myo.vibrate();
    if (speed > 0) speed--;
    break;
  default:
    break;
  }
}
*/